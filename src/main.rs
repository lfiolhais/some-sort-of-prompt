extern crate some_sort_of_prompt;

use some_sort_of_prompt::Prompt;

fn main() {
    let prompt = "Prompt... I guess... > ";

    loop {
        let mut my_prompt = Prompt::new(prompt.to_owned());

        print!("{}", my_prompt);
        let result = my_prompt.read();

        if result.len() == 0 {
            break;
        } else if result.len() > 1 {
            println!("Prompt object returned: {}", result);
        }
    }

    println!("Exited loop");
}
