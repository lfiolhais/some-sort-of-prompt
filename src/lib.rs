use std::env;
use std::fmt;
use std::fmt::Display;
use std::fs;
use std::io::{self, stderr, stdin, stdout, Write};
use std::path::Path;

use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;
use termion::raw::RawTerminal;

pub struct Prompt {
    prompt: String,
    cursor_position: usize,
    auto_complete_select: usize,
    binaries: Vec<String>,
}

impl Prompt {
    pub fn new(prompt: String) -> Self {
        let stderr = stderr();
        let mut stderr = stderr.lock().into_raw_mode().unwrap();

        let paths = match env::var("PATH") {
            Ok(paths) => paths,
            Err(e) => {
                write!(stderr, "{}", e).unwrap();
                return Prompt {
                    prompt,
                    cursor_position: 0,
                    auto_complete_select: 0,
                    binaries: vec![],
                };
            }
        };

        let paths: Vec<&str> = paths.split(':').collect();
        let mut binaries: Vec<String> = vec![];

        // Read all binaries in path per the order
        for path in &paths {
            match Prompt::read_binaries(&mut binaries, path) {
                Ok(_) => {}
                Err(e) => {
                    write!(stderr, "{}", e).unwrap();
                }
            };
        }

        Prompt {
            prompt,
            cursor_position: 0,
            auto_complete_select: 0,
            binaries,
        }
    }

    fn update_cursor_and_redraw(
        &mut self,
        input: &mut Vec<char>,
        stdout: &mut RawTerminal<io::StdoutLock>,
    ) {
        input.remove(self.cursor_position);

        write!(stdout, "{}", termion::clear::AfterCursor).unwrap();

        // Print only the part of the command that changed
        input.iter().skip(self.cursor_position).for_each(|st_char| {
            write!(stdout, "{}", st_char).unwrap();
        });

        // Reset the cursor back to where the user was if there is a discrepancy between
        // the cursor position after the print and where the user actually was.
        if self.cursor_position != input.len() {
            write!(
                stdout,
                "{}",
                termion::cursor::Left((input.len() - self.cursor_position) as u16)
            )
            .unwrap();
        }
    }

    pub fn read(&mut self) -> String {
        let mut input: Vec<char> = vec![];

        let stdin = stdin();
        let stdout = stdout();
        let stderr = stderr();
        let mut stdout = stdout.lock().into_raw_mode().unwrap();
        let mut stderr = stderr.lock().into_raw_mode().unwrap();

        for c in stdin.keys() {
            match c.unwrap() {
                // Go to the beginning of the command
                Key::Ctrl('a') if !input.is_empty() && self.cursor_position != 0 => {
                    write!(
                        stdout,
                        "{}",
                        termion::cursor::Left(self.cursor_position as u16)
                    )
                    .unwrap();
                    self.cursor_position = 0;
                    self.auto_complete_select = 0;
                }
                // Go to the end of the command
                Key::Ctrl('e') if !input.is_empty() && self.cursor_position < input.len() => {
                    write!(
                        stdout,
                        "{}",
                        termion::cursor::Right((input.len() - self.cursor_position) as u16)
                    )
                    .unwrap();
                    self.cursor_position = input.len();
                    self.auto_complete_select = 0;
                }
                // Move Cursor to the left
                Key::Left if !input.is_empty() && self.cursor_position != 0 => {
                    write!(stdout, "{}", termion::cursor::Left(1)).unwrap();
                    self.cursor_position -= 1;
                    self.auto_complete_select = 0;
                }
                // Move Cursor to the right
                Key::Right if !input.is_empty() && self.cursor_position < input.len() => {
                    write!(stdout, "{}", termion::cursor::Right(1)).unwrap();
                    self.cursor_position += 1;
                    self.auto_complete_select = 0;
                }
                // Delete previous character in command in relation to the cursor
                Key::Backspace if !input.is_empty() && self.cursor_position != 0 => {
                    self.cursor_position -= 1;
                    write!(stdout, "{}", termion::cursor::Left(1)).unwrap();
                    self.update_cursor_and_redraw(&mut input, &mut stdout);
                    self.auto_complete_select = 0;
                }
                // Delete Current character
                Key::Delete
                    if !input.is_empty()
                        && self.cursor_position != 0
                        && self.cursor_position < input.len() =>
                {
                    self.update_cursor_and_redraw(&mut input, &mut stdout);
                    self.auto_complete_select = 0;
                }
                // Autocomplete
                Key::Char('\t') if self.auto_complete_select != 0 || input.is_empty() => {
                    // Delete previous draw
                    if !input.is_empty() {
                        let binary = self.binaries[self.auto_complete_select - 1].clone();
                        for _ in 0..binary.len() {
                            self.cursor_position -= 1;
                            write!(stdout, "{}", termion::cursor::Left(1)).unwrap();
                            self.update_cursor_and_redraw(&mut input, &mut stdout);
                        }
                    }

                    let binary = self.binaries[self.auto_complete_select].clone();

                    for character in binary.chars() {
                        self.print_char(character, &mut input, &mut stdout);
                    }
                    self.auto_complete_select += 1;
                }
                // EOF
                Key::Ctrl('d') => break,
                // Replay prompt
                Key::Char('\n') => {
                    input.push('\n');
                    break;
                }
                // Store character
                // Ignore tabs
                Key::Char(a) if a != '\t' => {
                    self.print_char(a, &mut input, &mut stdout);
                    self.auto_complete_select = 0;
                }
                // Ignore all other key events
                _ => {}
            }

            stdout.flush().unwrap();
        }
        // New Line in Raw mode
        write!(
            stdout,
            "\r\n{}",
            termion::cursor::Left((self.prompt.len() + input.len()) as u16)
        )
        .unwrap();

        input.into_iter().collect()
    }

    fn read_binaries(binaries: &mut Vec<String>, path: &str) -> io::Result<()> {
        let dir = Path::new(path);
        if dir.is_dir() {
            for entry in fs::read_dir(dir)? {
                let entry = entry?;
                let path_tmp = entry.path();
                if path_tmp.is_dir() {
                    Self::read_binaries(binaries, path_tmp.to_str().unwrap())?;
                } else {
                    binaries.push(entry.file_name().into_string().unwrap());
                }
            }
        }

        Ok(())
    }

    fn print_char(
        &mut self,
        a: char,
        input: &mut Vec<char>,
        stdout: &mut RawTerminal<io::StdoutLock>,
    ) {
        input.insert(self.cursor_position, a);
        self.cursor_position += 1;

        // Print only the part of the command that changed
        input
            .iter()
            .skip(self.cursor_position - 1)
            .for_each(|st_char| {
                write!(stdout, "{}", st_char).unwrap();
            });

        // Reset the cursor back to where the user was if there is a discrepancy between
        // the cursor position after the print and where the user actually was.
        if self.cursor_position != input.len() {
            write!(
                stdout,
                "{}",
                termion::cursor::Left((input.len() - self.cursor_position) as u16)
            )
            .unwrap();
        }
    }
}

impl Display for Prompt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.prompt)?;
        stdout().flush().unwrap();
        Ok(())
    }
}
